#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2016年4月16日

' Models for course'

__author__ = 'Say Magic'

class Course(object):

    @property
    def course_id(self):
        return int(self._course_id)

    @course_id.setter
    def course_id(self, value):
        if isinstance(value, int):
            self._course_id = value
        elif isinstance(value, basestring):
            try:
                self._course_id = value.split("/")[-1]
            except Exception as e:
                self._course_id = -1
        else:
            self._course_id = -1

    def __str__(self):
        return 'Course object (name: %s) (course_id: %s) (img: %s) (des: %s)' % (self.name, self._course_id, self.img, self.des)

    def __init__(self):
        pass

    def to_string(self):
        return u'Course object (name: %s) (course_id: %s) (img: %s) (des: %s)' % (self.name, self._course_id, self.img, self.des)


class Student(object):

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be an integer!')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self._score = value

class VideoInfo(object):

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __str__(self):
        return 'VideoInfo object (name: %s) (url: %s)' % (self.name, self.url)

    def to_string(self):
        return u'VideoInfo object (name: %s) (url: %s)' % (self.name, self.url)
