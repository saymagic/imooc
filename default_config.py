#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2016年4月16日

' a default config module '

__author__ = 'Say Magic'

remote = {
    'host': 'http://www.imooc.com/',
    'course_list': 'http://www.imooc.com/course/list?page=',
    'mediainfo': 'http://www.imooc.com/course/ajaxmediainfo/?mid=',
    'learn_url': 'http://www.imooc.com/learn/',
    'max_page': 27,
    'type': 2
}

local = {
    'download_path': '/Users/ywd/tmp'
}

mail = {
    'smtp_host': 'smtp.126.com',
    'smtp_port': '',
    'user_name': 'docker009@126.com',
    'user_pwd': 'docker009',
    'mail_to': 'ilanyy@126.com'
}