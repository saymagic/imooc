#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2016年4月17日

' the mail module '

__author__ = 'Say Magic'

from email import encoders
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
import smtplib
import default_config as config
import sys, util
reload(sys)
sys.setdefaultencoding('utf-8')
import time, threading

def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((
        Header(name, 'utf-8').encode(),
        addr.encode('utf-8') if isinstance(addr, unicode) else addr))

def send_mail(title,context, to_addr=config.mail['mail_to']):
    t = threading.Thread(target=send_mail_directly, args=(title, context, to_addr))
    t.start()
    # send_mail_directly(title, context, to_addr)

def send_mail_directly(title,context, to_addr):
    # print u'Sending mail: %s   %s  %s' % (util.get_str(title), util.get_str(context), util.get_str(to_addr))
    title = util.get_unicode(title)
    context = util.get_unicode(context)
    from_addr = config.mail['user_name']
    password = config.mail['user_pwd']
    smtp_server = config.mail['smtp_host']

    msg = MIMEText(u'%s' % context, 'plain', 'utf-8')
    msg['From'] = _format_addr(u'Imooc Spider <%s>' % from_addr)
    msg['To'] = _format_addr(u'<%s>' % to_addr)
    msg['Subject'] = Header(u'%s' % title, 'utf-8').encode()

    server = smtplib.SMTP(smtp_server, 25)
    server.set_debuglevel(1)
    server.login(from_addr, password)
    server.sendmail(from_addr, [to_addr], msg.as_string())
    server.quit()


if __name__ == '__main__':
    send_mail(u"这是测试主题", u"这是征文")
