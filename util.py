#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2016年4月17日

' the util module '

__author__ = 'Say Magic'

import sys

def get_str(s):
    return s.encode('utf-8') if isinstance(s, unicode) else s

def get_unicode(s):
    return s.decode('utf-8') if isinstance(s, str) else s

if __name__=='__main__':
    pass