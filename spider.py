#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2016年4月16日

'the spider module '

__author__ = 'Say Magic'

import requests, json, os, mail
import default_config as config
from models import Course, VideoInfo
from bs4 import BeautifulSoup, element
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def get_bs_by_url(url):
    header_info = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1581.2 Safari/537.36',
        'Host': 'www.imooc.com',
        'Origin': 'http://www.imooc.com',
        'Connection': 'keep-alive',
        'Referer': 'http://www.imooc.com',
    }
    r = requests.get(url, headers=header_info)
    return BeautifulSoup(r.text, "html.parser")


def list_all_videos_by_courseid(id):
    result = []
    bsObj = get_bs_by_url(config.remote['learn_url'] + str(id))
    videos = bsObj.findAll("a", {"class":"J-media-item studyvideo"})
    for video in videos:
        if isinstance(video, element.Tag) and 'href' in video.attrs:
            video_id = video.attrs['href'].split("/")[-1];
            video_json = requests.get(config.remote['mediainfo'] + video_id).json()
            videoObj = VideoInfo(video_json['data']['result']['name'],video_json['data']['result']['mpath'][config.remote['type']])
            result.append(videoObj)
    return result


def list_all_course_by_pageid(id):
    bsObj = get_bs_by_url(config.remote['course_list'] + str(id))
    courses = bsObj.findAll("li", {"class": "course-one"})
    for course in courses:
        courseObj = Course()
        try:
            for child in course.descendants:
                if isinstance(child, element.Tag):
                    if child.name == 'a' and 'href' in child.attrs:
                        courseObj.course_id = child.attrs['href']
                    if child.name == 'span' and 'class' not in child.attrs:
                        courseObj.name = child.getText()
                    if child.name == 'img' and 'src' in child.attrs:
                        courseObj.img = child.attrs['src']
                    if child.name == 'p' and 'class' in child.attrs :
                        courseObj.des = child.getText()
            if courseObj.course_id > 0:
                print courseObj
                videos = list_all_videos_by_courseid(courseObj.course_id)
                courseObj.videos = videos
                handle_course(courseObj, id)
        except Exception as e:
            mail.send_mail(u'Failed Course %s' % courseObj.name, str(e) )
        else:
            mail.send_mail(u'Success : Course %s  ' % courseObj.name, courseObj.to_string())
        print '---------------------------------------------------------------'

def handle_course(course, page_id):
    videos = course.videos
    for video_info in videos:
        download_video(video_info, course.name, page_id)

def download_video(video_info, course_name, page_id):
    try:
        course_root_path = os.path.join(get_unicode(config.local['download_path']), os.path.join(get_unicode(str(page_id)), course_name))
        if not os.path.exists(get_str(course_root_path)):
            os.makedirs(get_str(course_root_path))
        local_filename = os.path.join(course_root_path, get_unicode(video_info.name)) + u".mp4"
        # NOTE the stream=True parameter
        r = requests.get(video_info.url, stream=True)
        with open(get_str(local_filename), 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
    except Exception as e:
        mail.send_mail(u'Failed(%s) in Course  (%s)' % (video_info.name, course_name), str(e), u"docker009@126.com")
    else:
        mail.send_mail(u'Success(%s) in Course  (%s)' % (video_info.name, course_name), "!", u"docker009@126.com")


def start():
    r = requests.get('http://www.imooc.com/course/ajaxmediainfo/?mid=5210')
    js = r.json()
    print js['data']['result']['name']

def get_str(s):
    return s.encode('utf-8') if isinstance(s, unicode) else s

def get_unicode(s):
    return s.decode('utf-8') if isinstance(s, str) else s

if __name__ == '__main__':
    # list_all_course_by_pageid(2)
    # list_all_videos_by_courseid(40)
    for i in range(config.remote['max_page']):
        list_all_course_by_pageid(i)
    # s = Student()
    # s.score = 30
    # print s.score